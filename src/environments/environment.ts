// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  BaseURL: 'http://api.themoviedb.org/3',
  firebaseConfig: {
    apiKey: "AIzaSyAtp7u4QXQR87gQeocw9yeY7rcLKFJmYZc",
    authDomain: "portafolio-movieapp.firebaseapp.com",
    projectId: "portafolio-movieapp",
    storageBucket: "portafolio-movieapp.appspot.com",
    messagingSenderId: "956471190995",
    appId: "1:956471190995:web:a6a2c92f607e59c187fb26",
    measurementId: "G-L06D0PR993"
  }
};
 
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
