import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appColorError]'
})
export class ColorErrorDirective implements OnInit {

  constructor(
    public element: ElementRef,
    public renderer: Renderer2
  ) { }
  
  ngOnInit(): void {
  
    this.renderer.setStyle(this.element.nativeElement, 'color', "green")
    
  }

}
