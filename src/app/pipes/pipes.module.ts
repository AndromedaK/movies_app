import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PosterPipe } from './poster.pipe';
import { PosterDetailPipe } from './poster-detail.pipe';



@NgModule({
  declarations: [PosterPipe, PosterDetailPipe],
  imports: [
    CommonModule
  ],
  exports:[
    PosterPipe,
    PosterDetailPipe
  ]
})
export class PipesModule { }
