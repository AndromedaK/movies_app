import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/components/auth/auth.service';
import { User } from 'src/app/models/user';
import { StorageUserService } from 'src/app/services/storage-user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() public currentUser: User;
  private uid: string;

  constructor(
    private authService: AuthService,
    private router: Router,

  ) { }

  ngOnInit() {
  }

  onLogout(){
    this.authService.logoutFirebase().then((response)=>{
      console.log("logout exitoso");
      console.log(response);
      this.router.navigate(['auth/login'])
    }).catch(error=>{
      console.log('error logout --->', error);
    })
  }

  searchMovie = ( value: string ) => {
    value = value.trim();
    if(value.length === 0){
      return;
    }
    this.router.navigate(['/movie/search', value])
  }

}
