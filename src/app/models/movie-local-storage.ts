
import { MovieNow, OriginalLanguage } from "./movies-now-playing";

export class MovieLocal  implements MovieNow{
    adult: boolean;
    backdrop_path: string;
    genre_ids: number[];
    id: number;
    original_language: OriginalLanguage;
    original_title: string;
    overview: string;
    popularity: number;
    poster_path: string;
    release_date: Date;
    title: string;
    video: boolean;
    vote_average: number;
    vote_count: number;
    idCurrentUser: string    

}
