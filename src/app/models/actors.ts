export interface Actors {
    page:          number;
    results:       Actor[];
    total_pages:   number;
    total_results: number;
}

export interface Actor {
    adult:                boolean;
    gender:               number;
    id:                   number;
    known_for:            KnownFor[];
    known_for_department: KnownForDepartment;
    name:                 string;
    popularity:           number;
    profile_path:         string;
}
export interface KnownFor {
    backdrop_path:     string;
    first_air_date?:   Date;
    genre_ids:         number[];
    id:                number;
    media_type:        MediaType;
    name?:             string;
    origin_country?:   string[];
    original_language: OriginalLanguage;
    original_name?:    string;
    overview:          string;
    poster_path:       string;
    vote_average:      number;
    vote_count:        number;
    adult?:            boolean;
    original_title?:   string;
    release_date?:     Date;
    title?:            string;
    video?:            boolean;
}

export enum MediaType {
    Movie = "movie",
    Tv = "tv",
}

export enum OriginalLanguage {
    En = "en",
}

export enum KnownForDepartment {
    Acting = "Acting",
}
