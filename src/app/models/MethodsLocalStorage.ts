import { Observable } from "rxjs";

interface Methods {
    add( entity: any);
    get<T>():Observable<T[]>;
    delete(id:number);
}

export abstract class MethodsLocalStorage  implements Methods{
   abstract add(entity: any);
   abstract get<T>(): Observable<T[]>;  
   abstract delete(id: number);
}