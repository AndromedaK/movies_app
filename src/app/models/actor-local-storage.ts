import { Actor, KnownFor, KnownForDepartment } from "./actors";

export class ActorLocal implements Actor{
    adult: boolean;
    gender: number;
    id: number;
    known_for: KnownFor[];
    known_for_department: KnownForDepartment;
    name: string;
    popularity: number;
    profile_path: string;
    idCurrentUser: string
}