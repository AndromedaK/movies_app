import { Component, OnInit } from '@angular/core';
import { Actor } from 'src/app/models/actors';
import { ActorsService } from 'src/app/services/actors.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  actors: Actor[] = []

  constructor(
    private actorService:ActorsService
  ) { }

  ngOnInit() {
    this.actorService.getActors().subscribe((actors =>{
      this.actors = actors
    }))
  }

}
