import { Component, OnInit } from '@angular/core';
import { ActorLocal } from 'src/app/models/actor-local-storage';
import { Actor } from 'src/app/models/actors';
import { MovieLocal } from 'src/app/models/movie-local-storage';
import { MovieNow } from 'src/app/models/movies-now-playing';
import { StorageActorsService } from 'src/app/services/storage-actors.service';
import { StorageMoviesService } from 'src/app/services/storage-movies.service';
import { StorageUserService } from 'src/app/services/storage-user.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {

  public actorListLocal: ActorLocal[] = [];
  public actorList: Actor[] = [];

  public movieListLocal: MovieLocal[] = [];
  public movieList: MovieNow[] = [];

  constructor(
    private storageActorService : StorageActorsService,
    private storageUserService  : StorageUserService,
    private storageMovieService: StorageMoviesService
  ) { }

  ngOnInit() {

    this.storageActorService.refreshData.subscribe(()=>{
      this.getAllActors();
    })

    this.storageMovieService.refreshMovies.subscribe(()=>{
      this.getAllMovies();
    })

    this.getAllActors();
    this.getAllMovies();



  }


  public returnWithoutIdActor = (actors, actorActual: ActorLocal) => {
    
    /** RemoveID */
    const { idCurrentUser ,...restActorProperties } = actorActual;
    actors.push(restActorProperties);
    return actors;

  }

  public returnWithoutIdMovie= (movies, actorActual: MovieLocal) => {
    
    /** RemoveID */
    const { idCurrentUser ,...restActorProperties } = actorActual;
    movies.push(restActorProperties);
    return movies;

  }


  private getAllActors(){
    this.storageActorService.get<ActorLocal>().subscribe((actorlistLocal)=>{
      this.actorListLocal = [...actorlistLocal]; 
      this.actorList  = this.actorListLocal.reduce(this.returnWithoutIdActor, []);
    });
  }


  private getAllMovies(){
    this.storageMovieService.get<MovieLocal>().subscribe((movielistLocal:MovieLocal[])=>{
      this.movieListLocal = [...movielistLocal]; 
      this.movieList = movielistLocal.reduce(this.returnWithoutIdMovie, []);
    });
  }
  


}
