import { Component, Input, OnInit } from '@angular/core';
import { Actor } from 'src/app/models/actors';
import { StorageActorsService } from 'src/app/services/storage-actors.service';

@Component({
  selector: 'app-actor-favorite-card',
  templateUrl: './actor-favorite-card.component.html',
  styleUrls: ['./actor-favorite-card.component.scss']
})
export class ActorFavoriteCardComponent implements OnInit {

  @Input() actors: Actor[]; 
  
  constructor(
    private storageActorService: StorageActorsService
  ) { }

  ngOnInit() {
  }

  deleteOfFavoriteActors(id:number){
    this.storageActorService.delete(id)
  }

  goToactor(actor: any){
    
  }
}
