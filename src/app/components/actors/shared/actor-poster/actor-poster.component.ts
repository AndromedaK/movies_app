import { Component, Input, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ActorLocal } from 'src/app/models/actor-local-storage';
import { Actor } from 'src/app/models/actors';
import { StorageActorsService } from 'src/app/services/storage-actors.service';
import { StorageUserService } from 'src/app/services/storage-user.service';

@Component({
  selector: 'app-actor-poster',
  templateUrl: './actor-poster.component.html',
  styleUrls: ['./actor-poster.component.scss']
})
export class ActorPosterComponent implements OnInit {
  
  @Input() actors: Actor[]; 

  constructor(
    private storageActorService : StorageActorsService,
    private storageUserService  : StorageUserService,
    private _snackbar: MatSnackBar
  ) { }

  ngOnInit() {
  }

  
  addActor(actor: Actor){
    this.storageUserService.currentUSer().then(resp=>{
      const { uid } = resp;
      const actorLocal: ActorLocal = {...actor, idCurrentUser: uid }
      console.log("se esta guardando o siguiente -->", actorLocal)
      this.storageActorService.add(actorLocal);
      this._snackbar.open(`Se ha añadido a tu lista de actores favoritos ${actorLocal.name}`,"Ok",{
        verticalPosition: 'top',
        duration: 2000,
        panelClass: ['green-snackbar']
      })  
    })
  }

  goToactor(actor: Actor){
    
  }
}
