import { Component, OnInit } from '@angular/core';
import { Actor } from 'src/app/models/actors';
import { ActorsService } from 'src/app/services/actors.service';

@Component({
  selector: 'app-all-actors',
  templateUrl: './all-actors.component.html',
  styleUrls: ['./all-actors.component.scss']
})
export class AllActorsComponent implements OnInit {
  
  public animation: boolean = true; 
  public actors: Actor[] = []

  constructor(
    private actorService: ActorsService
  ) { }

  ngOnInit() {
    this.actorService.getActors().subscribe((actors =>{
      this.actors = actors
      this.animation = false; 
    }))
  }



}
