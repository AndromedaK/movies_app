import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MovieNow, MoviesNow } from 'src/app/models/movies-now-playing';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-all-movies',
  templateUrl: './all-movies.component.html',
  styleUrls: ['./all-movies.component.scss']
})
export class AllMoviesComponent implements OnInit, OnDestroy{
  
  @HostListener('window:scroll', [''])
  onScroll(){
    const position = (document.documentElement.scrollTop || document.body.scrollTop) + 1300;
    const max      = (document.documentElement.scrollHeight || document.body.scrollHeight)
    if(position>max){
      if( this.movieService.loading ){return;}
      this.movieService.getMoviesNowPlaying().subscribe((movies)=>{
          this.movies.push(...movies);
      })
    }
  }

  public movies: MovieNow[];
  public moviesSlideShow: MovieNow[];
  public animation: boolean = true;

  searchControl: FormControl = new FormControl('')

  constructor(
    private movieService: MoviesService
  ) {}
  
  ngOnDestroy(): void {
    this.movieService.resetPage();
  }

  ngOnInit(): void {
    this.animation = true
    this.movieService.getMoviesNowPlaying().subscribe((movies: MovieNow[])=>{  
      this.movies = movies; 
      this.moviesSlideShow = movies; 
      this.animation = false;
    })


  }

 

}
