import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MovieLocal } from 'src/app/models/movie-local-storage';
import { MovieNow } from 'src/app/models/movies-now-playing';
import { StorageMoviesService } from 'src/app/services/storage-movies.service';

@Component({
  selector: 'app-movie-favorite-card',
  templateUrl: './movie-favorite-card.component.html',
  styleUrls: ['./movie-favorite-card.component.scss']
})
export class MovieFavoriteCardComponent implements OnInit {

  @Input() movies: MovieNow[]
  constructor(
    private storageMovieService:StorageMoviesService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  goToMovie(movie:MovieNow){
    this.router.navigate(['movie', movie.id])
    
  }
  deleteOfFavoriteMovies(id:number){
    this.storageMovieService.delete(id) 
  }

}
