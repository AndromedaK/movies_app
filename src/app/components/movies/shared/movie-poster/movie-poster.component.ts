import { Component, Input, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { MovieLocal } from 'src/app/models/movie-local-storage';
import { MoviesNow, MovieNow } from 'src/app/models/movies-now-playing';
import { StorageMoviesService } from 'src/app/services/storage-movies.service';
import { StorageUserService } from 'src/app/services/storage-user.service';

@Component({
  selector: 'app-movie-poster',
  templateUrl: './movie-poster.component.html',
  styleUrls: ['./movie-poster.component.scss']
})
export class MoviePosterComponent implements OnInit {

  @Input() movies: MovieNow[];
  constructor(
    private router: Router,
    private storageMovieService: StorageMoviesService,
    private _snackbar: MatSnackBar,
    private storageUserService: StorageUserService
  ) { }

  ngOnInit() {
  } 

  goToMovie = (movie: MovieNow) => {
    this.router.navigate(['movie', movie.id])
  }

  addToFavorite = (movie: MovieNow) => {
    this.storageUserService.currentUSer().then(resp=>{
      const { uid } = resp;
      const movieLocal: MovieLocal = {...movie, idCurrentUser: uid }
      console.log("se esta guardando o siguiente -->", movieLocal)
      this.storageMovieService.add(movieLocal);
      this._snackbar.open(`La película ${movie.title} se ha añadido a favoritas`, "Ok",{
        verticalPosition: 'top',
        duration: 2000,
        panelClass: ['blue-snackbar']
      });
    })

  }
}
