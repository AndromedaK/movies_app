import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Cast } from 'src/app/models/credits';
import { MovieDetails } from 'src/app/models/movie-details';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss']
})
export class MovieDetailComponent implements OnInit {
  
  public id:string;
  public movieDetails: MovieDetails;
  public load:boolean =  true;
  public casting: Cast[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private movieService: MoviesService,
    private router: Router,
    private location: Location

  ) { }

  ngOnInit() {
    this.load = true
    this.activatedRoute.params.subscribe((response)=>{
      const {id} = response;
      this.id    = id;
    })

    this.movieService.getMovieDetail(this.id).subscribe((movie)=>{
      if(!movie){
        this.location.back()
        return;
      }
      this.movieDetails = movie;
      this.load = false;
    })

    this.movieService.getCasting(this.id).subscribe((casting)=>{
      if(!casting){
        this.location.back()
        return;
      }
      this.casting = casting.filter( m => m.profile_path !== null );
    })
  }

  back(){
    this.location.back();
  }

}
