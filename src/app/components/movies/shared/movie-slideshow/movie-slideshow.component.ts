import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { MovieNow } from 'src/app/models/movies-now-playing';
import Swiper from 'swiper';

@Component({
  selector: 'app-movie-slideshow',
  templateUrl: './movie-slideshow.component.html',
  styleUrls: ['./movie-slideshow.component.scss']
})

export class MovieSlideshowComponent implements OnInit, AfterViewInit {

  @Input() movies: MovieNow[];
  public movieSwiper: Swiper;

  constructor() { }

  ngAfterViewInit(): void {
    this.movieSwiper = new Swiper('.swiper-container', {
      slidesPerView: 3,
      spaceBetween: 30,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    });
  }

  ngOnInit() {
  }

  onSlideNext = () =>{
    this.movieSwiper.slideNext();
  }

  onSlidePrevious = () =>{
    this.movieSwiper.slidePrev();
  }

}
