import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { Cast } from 'src/app/models/credits';
import Swiper from 'swiper';

@Component({
  selector: 'app-casting-slide-show',
  templateUrl: './casting-slide-show.component.html',
  styleUrls: ['./casting-slide-show.component.scss']
})
export class CastingSlideShowComponent implements OnInit,AfterViewInit {

  @Input() casting: Cast[];

  constructor() { }

  ngAfterViewInit(): void {
    const swiper = new Swiper('.swiper-container',{
      slidesPerView: 5.3,
      freeMode: true,
      spaceBetween: 15
    });

  }

  ngOnInit() {
    console.log(this.casting);
  }

}
