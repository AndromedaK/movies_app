import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieNow } from 'src/app/models/movies-now-playing';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-search-movie',
  templateUrl: './search-movie.component.html',
  styleUrls: ['./search-movie.component.scss']
})
export class SearchMovieComponent implements OnInit {
  
  movies: MovieNow[] = []
  text:string = '';

  constructor(
    private activedRoute: ActivatedRoute,
    private movieService: MoviesService
  ) { }

  ngOnInit() {
    this.activedRoute.params.subscribe((params)=>{
      const {text} = params;
      this.text = text; 
      this.movieService.searchMovie(text).subscribe((movies)=>{
        this.movies = movies; 
      })
    
    })
  }

}
