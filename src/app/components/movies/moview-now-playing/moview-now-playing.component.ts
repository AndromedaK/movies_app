import { Component, OnInit } from '@angular/core';
import { MovieNow } from 'src/app/models/movies-now-playing';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-moview-now-playing',
  templateUrl: './moview-now-playing.component.html',
  styleUrls: ['./moview-now-playing.component.scss']
})
export class MoviewNowPlayingComponent implements OnInit {

  public moviesSlideShow: MovieNow[];
  public animation: boolean = true;
  
  constructor(
    private movieService: MoviesService
  ) { }

  ngOnInit() {
    this.animation = true
    this.movieService.getMoviesNowPlaying().subscribe((movies: MovieNow[])=>{  
      this.moviesSlideShow = movies; 
      this.animation = false;
    })
  }

}
