/**MODULES */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { MaterialModule } from 'src/app/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';


/**COMPONENTS */
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

import { environment } from "../../../environments/environment";
import { LoaderModule } from 'src/app/shared/loader/loader.module';
import { ColorErrorDirective } from 'src/app/directive/color-error.directive';
import { StorageUserService } from 'src/app/services/storage-user.service';
import { PassswordDirective } from 'src/app/directive/validators/passsword.directive';





@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    ColorErrorDirective,
    PassswordDirective
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    LoaderModule, 
    AngularFirestoreModule
  ],
  providers: [
    StorageUserService
  ]
})
export class AuthModule { }
