import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { MatSnackBar } from '@angular/material';;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  
  constructor(
    private fireAuth:  AngularFireAuth,
    private _snackBar: MatSnackBar
  ) { 
  }


  async loginFirebase(email:string, password:string){
    try {
      const response = await this.fireAuth.auth.signInWithEmailAndPassword(email, password);
      localStorage.setItem('email', response.user.email);
      return response; 
    } catch (error) {
      console.log('error auth --> ', error)
      this._snackBar.open(error.message, 'cerrar', {
        duration: 2000,
      });
    }
  }

  async registerFirebase(email: string, password: string){ 
    try {
      const response = await this.fireAuth.auth.createUserWithEmailAndPassword(email,password);     
      return response.user;
        
    } catch (error) {
      console.log('error auth --> ', error)
      this._snackBar.open(error.message, 'cerrar', {
        duration: 2000,
      });
    }
  }

  async logoutFirebase(){
    try {
      const response = await this.fireAuth.auth.signOut();
      localStorage.clear();
      return response;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  isLoggedFirebase(){ 
    const emailStored = localStorage.getItem('email');

    if(emailStored){
      return true;
    }
    else{
      return false;
    }
  }


}
