import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { passwordValidation } from 'src/app/directive/validators/passsword.directive';

/*SERVICE**/
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  /**VARIABLES */
  formLogin: FormGroup;
  public animation: boolean = false;
  hide:boolean = true;


  /**GETTERS */
  get emailControl(){ return this.formLogin.get('email') };
  get passwordControl() { return this.formLogin.get('password') };
 
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  private buildForm = () =>{
    this.formLogin = this.fb.group(
      {
        email: ['nahuel@gmail.com',[Validators.required, Validators.email]],
        password: ['@Nahuel123', [Validators.required, passwordValidation()]]
      }
    )
  }

  public login = () => {
    this.animation = true;
    if(this.formLogin.valid){
      this.authService.loginFirebase(this.emailControl.value, this.passwordControl.value).then(response => {
        if(response.user){
          localStorage.setItem('uid', response.user.uid)
          this.router.navigate(['./home'])
        }
      }).catch((error)=>{
        this.animation = false;  
        this._snackBar.open(error.message, 'Cerrar', {
          duration: 2000,
        });
      })
    }        
  }




}
