import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { User } from '../../../models/user';
import { passwordValidation } from 'src/app/directive/validators/passsword.directive';
import { StorageUserService } from 'src/app/services/storage-user.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  /**GETTERS */
  get emailControl(){return this.formRegister.get('email')};
  get passwordControl(){return this.formRegister.get('password')};
  get nameControl(){return this.formRegister.get('name')};

  /**VARIABLES */
  formRegister: FormGroup;
  
  constructor(
    private fb:FormBuilder,
    private authService: AuthService,
    private userService: StorageUserService,
    private router: Router,
    private _snackbar:MatSnackBar
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  private buildForm = ():void => {
    this.formRegister =  this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, passwordValidation()]],
    })
  }

  register = ():void => {
  
    if(this.formRegister.valid){

      this.authService.registerFirebase(this.emailControl.value, this.passwordControl.value).then((response)=>{
         
        let user: User = {
          name: this.nameControl.value,
          email: response.email,
          uid: response.uid
        };

        this.userService.createUser(user).then((response)=>{
          this.router.navigate(['auth/login']);
          this._snackbar.open(`Su usuario ha ${user.name} sido registrado satisfactoriamente`, "Ok",{
            verticalPosition: 'top',
            duration: 2000
          });

        }).catch((error) => {
          this._snackbar.open(error.message, "Cerrar",{
            verticalPosition: 'top',
            duration: 2000
          });
        });


      })

    }


  }
    


}
