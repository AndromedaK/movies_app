import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { User } from 'src/app/models/user';
import { StorageUserService } from 'src/app/services/storage-user.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit {
  
  public uid: string;
  public uidLocal: string = 'uid';
  public currrentUser: User;

  public perfilForm: FormGroup;

  /** GETTERS */
  
  // get emailControl(){ return this.perfilForm.get('email')}
  get nameControl(){ return this.perfilForm.get('name')}

  constructor(
    private storageUserService:StorageUserService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.initData();
  }

  buildForm(){
    this.perfilForm = this.fb.group({
      name: [null],
    })
  }

  initData(){
    const uidSessionStorage = localStorage.getItem(this.uidLocal);
    this.uid = uidSessionStorage;            
    this.storageUserService.getUser(this.uid).subscribe((user)=>{
      this.currrentUser = {...user};
      this.buildForm();
      this.setValuesForm();
    })
  }
  
  setValuesForm(){
    this.nameControl.setValue(this.currrentUser.name)
    // this.emailControl.setValue(this.currrentUser.email)
  }

}
