
/**MODULES */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from "../../material/material.module";
import { HttpClientModule } from "@angular/common/http";
import { RatingModule } from 'ng-starrating';
import { PipesModule } from "../../pipes/pipes.module";
import { LayoutRoutingModule } from './layout-routing.module';
import { LoaderModule } from 'src/app/shared/loader/loader.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/**COMPONENTS */
import { HomeComponent } from '../home/home.component';
import { AllMoviesComponent } from '../movies/all-movies/all-movies.component';
import { MoviePosterComponent } from '../movies/shared/movie-poster/movie-poster.component';
import { MovieSlideshowComponent } from '../movies/shared/movie-slideshow/movie-slideshow.component';
import { HeaderComponent } from 'src/app/shared/header/header.component';
import { LayoutComponent } from './layout.component';
import { FooterComponent } from 'src/app/shared/footer/footer.component';
import { MovieDetailComponent } from '../movies/shared/movie-detail/movie-detail.component';
import { CastingSlideShowComponent } from '../movies/shared/casting-slide-show/casting-slide-show.component';
import { PerfilComponent } from '../perfil/perfil.component';
import { MoviewNowPlayingComponent } from '../movies/moview-now-playing/moview-now-playing.component';
import { SearchMovieComponent } from '../movies/search-movie/search-movie.component';
import { AllActorsComponent } from '../actors/all-actors/all-actors.component';
import { ActorPosterComponent } from '../actors/shared/actor-poster/actor-poster.component';
import { FavoritesComponent } from '../favorites/favorites.component';
import { ActorFavoriteCardComponent } from '../actors/shared/actor-favorite-card/actor-favorite-card.component';
import { MovieFavoriteCardComponent } from '../movies/shared/movie-favorite-card/movie-favorite-card.component';

@NgModule({
  declarations: [
    HomeComponent,
    AllMoviesComponent,
    MoviePosterComponent,
    MovieSlideshowComponent,
    HeaderComponent,
    LayoutComponent,
    FooterComponent,
    MovieDetailComponent,
    CastingSlideShowComponent,
    PerfilComponent,
    MoviewNowPlayingComponent,
    SearchMovieComponent,
    AllActorsComponent,
    ActorPosterComponent,
    FavoritesComponent,
    ActorFavoriteCardComponent,
    MovieFavoriteCardComponent
  ],
  imports: [
    MaterialModule,
    CommonModule,
    LayoutRoutingModule,
    HttpClientModule,
    RatingModule,
    PipesModule,
    LoaderModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class LayoutModule { }
