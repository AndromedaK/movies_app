/**MODULES */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/guards/auth.guard';

/**COMPONENTS */
import { AllActorsComponent } from '../actors/all-actors/all-actors.component';
import { FavoritesComponent } from '../favorites/favorites.component';
import { HomeComponent } from '../home/home.component';
import { AllMoviesComponent } from '../movies/all-movies/all-movies.component';
import { MoviewNowPlayingComponent } from '../movies/moview-now-playing/moview-now-playing.component';
import { SearchMovieComponent } from '../movies/search-movie/search-movie.component';
import { MovieDetailComponent } from '../movies/shared/movie-detail/movie-detail.component';
import { PerfilComponent } from '../perfil/perfil.component';
import { LayoutComponent } from './layout.component';


const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'home', 
        component: HomeComponent,
        canActivateChild: [AuthGuard],
      },
      {
        path: 'list-movies', 
        component: AllMoviesComponent,
        canActivateChild: [AuthGuard],
      },
      {
        path: 'now-playing', 
        component: MoviewNowPlayingComponent,
        canActivateChild: [AuthGuard],
      },
      {
        path: 'movie/:id',
        component: MovieDetailComponent,
        canActivateChild: [AuthGuard]
      },
      {
        path: 'movie/search/:text',
        component: SearchMovieComponent,
        canActivateChild: [AuthGuard]
      },
      {
        path: 'list-actors',
        component: AllActorsComponent,
        canActivateChild: [AuthGuard]
      },
      {
        path:'perfil',
        component: PerfilComponent,
        canActivateChild: [AuthGuard]
      },
      {
        path: 'favorites',
        component: FavoritesComponent,
        canActivateChild: [AuthGuard]
      },
      {
        path: '**',
        redirectTo: '/home'
      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
