import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { StorageUserService } from 'src/app/services/storage-user.service';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {


  private uid: string;
  public currentUser:  User
  public loader:boolean =  true;

  constructor(
    private storageUserService: StorageUserService,
  ) { }

  ngOnInit() {
    const uidSessionStorage = localStorage.getItem('uid');
    this.uid = uidSessionStorage;            
    this.storageUserService.getUser(this.uid).subscribe((user)=>{
      this.currentUser = {...user};
      this.loader = false;
    })
  }

}
