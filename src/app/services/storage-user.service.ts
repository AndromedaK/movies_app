import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from "@angular/fire/firestore";
import * as firebase from 'firebase';
import { Observable, of } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class StorageUserService {

  public userBd: any;

  constructor(
    private firestore: AngularFirestore,
    private fireAuth:  AngularFireAuth,
    
  ) { }

  async createUser(newUser: User): Promise<string>{
    try {      
      const user = await this.firestore.collection('users').add(newUser);
      console.log("reponse createUser -->", user); 
      return user.id; 

    } catch (error) {
      console.log("error create user", error); 
    }
  }

  getUser(uid: string): Observable<User> {
    return new Observable(observer => {
      this.firestore.firestore.collection('users').where('uid', '==', uid).get().then(querySnapshot => {
        querySnapshot.forEach(resp => {
          this.userBd = resp.data();
        })
        observer.next(this.userBd);
        observer.complete();
      }).catch(error => {
        console.log('error call service firestore -->', error);
        observer.error(error);
        observer.complete();
      })
    });
  }
  
  async currentUSer() {
    const currentUSerResp = this.fireAuth.auth.currentUser;
    console.log('currentUser service -->', currentUSerResp);
    return currentUSerResp; 
  }

  async updateUser(){
  
  }
}
