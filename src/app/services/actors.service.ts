import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Actors, Actor } from '../models/actors';

@Injectable({
  providedIn: 'root'
})
export class ActorsService {

  private actorPage:number = 1;
  private BaseURL:string = environment.BaseURL;
  private loading: boolean = false; 
  
  get params(){
    return {
      api_key: "0d8aa52a1ac6aa97b41f7f2a1af9f5f8",
      language: 'es-ES',
      page: this.actorPage.toString()
    }
  }

  constructor(
    private http: HttpClient
  ) { }
  
  getActors(): Observable<Actor[]>{

    // Si esta en true devuelve un observable de arreglo vacío
    if(this.loading){
      return of([]);
    }
    return this.http.get<Actors>( `${this.BaseURL}/person/popular`, { params: this.params } )
      .pipe(
        map(resp => resp.results)
      )
  }




}
