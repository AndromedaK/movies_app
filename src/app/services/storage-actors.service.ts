import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { of } from 'rxjs/internal/observable/of';
import { switchMap } from 'rxjs/operators';
import { ActorLocal } from '../models/actor-local-storage';
import { MethodsLocalStorage } from '../models/MethodsLocalStorage';

@Injectable({
  providedIn: 'root'
})
export class StorageActorsService extends MethodsLocalStorage {


  add(actor: ActorLocal) {
    let actortest =  this._actorList.find( actorL =>  actorL.name === actor.name );
    if(!actortest){
      console.log("este actor no existe", actortest) 
      this._actorList.push(actor); 
      localStorage.setItem('actorList', JSON.stringify(this._actorList))
    }
  }

  get<ActorLocal>(): Observable<ActorLocal[]> {
    let  actorList =  localStorage.getItem('actorList');
    return of(JSON.parse(actorList));
  }
  delete(id: number) {
    if(this._actorList){
      this._actorList = this._actorList.filter(actor => actor.id !== id);
      localStorage.setItem('actorList', JSON.stringify(this._actorList))
      this._refreshData$.next();
    }
    else{
      let  actorList:string =  localStorage.getItem('actorList');
      let actorListParsed = JSON.parse(actorList);
      this._actorList = actorListParsed.filter(actor => actor.id !== id);
      localStorage.setItem('actorList', JSON.stringify(this._actorList))
      this._refreshData$.next();
    }
    
  }

  private _actorList: ActorLocal[] = [];
  private readonly _refreshData$ = new Subject<void>();

  get refreshData(){
    return this._refreshData$;
  }

  constructor(){
    super()
  }

 

  getFavoriteActorLocalStorage(): Observable<ActorLocal[]>{
    let  actorList =  localStorage.getItem('actorList');
    return of(JSON.parse(actorList));
  }


  deleteFavoriteActorLocalStorage(id:number){

    
  }
}
