import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MovieNow, MoviesNow } from '../models/movies-now-playing';
import { catchError, map, tap } from "rxjs/operators";
import { MovieDetails } from '../models/movie-details';
import { Cast, Credits } from '../models/credits';

@Injectable({
  providedIn: 'root'
})

export class MoviesService {
  
  private moviePage:number = 1;
  private BaseURL:string = environment.BaseURL;
  private ApiURL:string = `${this.BaseURL}/movie`; 
  public loading: boolean = false;

  constructor(
    private http: HttpClient
  ) {  }

  get params(){
    return {
      api_key: "0d8aa52a1ac6aa97b41f7f2a1af9f5f8",
      language: 'es-ES',
      page: this.moviePage.toString()
    }
  }

  getMoviesNowPlaying():Observable<MovieNow[]> {
    
    // Si esta en true devuelve un observable de arreglo vacío
    if(this.loading){
      return of([]);
    }

    this.loading = true;
    return this.http.get<MoviesNow>(`${this.ApiURL}/now_playing`, { params: this.params}).pipe(
      map((resp)=> resp.results),
      tap(()=>{
        this.moviePage +=1;
        this.loading = false;
      })
    )
  }

  resetPage = () => {
    this.moviePage = 1; 
  }

  getMovieDetail(id:string){
    return this.http.get<MovieDetails>(`${this.ApiURL}/${id}`, {params: this.params} )
      .pipe(
        catchError( error =>  of(null)
        )
      )
  }


  getCasting(id:string): Observable<Cast[]>{
    return this.http.get<Credits>(`${this.ApiURL}/${id}/credits`,{params: this.params})
      .pipe(
        map(response => response.cast ),
        catchError( error => of([]))
      );
  }
  
  searchMovie( text:string): Observable<MovieNow[]>{
    const params = {...this.params, page: '1', query: text}; 
    return this.http
      .get<MoviesNow>(`${this.BaseURL}/search/movie`, { params })
      .pipe(
        map( resp => resp.results )
      )
  }

}
