import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { MethodsLocalStorage } from '../models/MethodsLocalStorage';
import { MovieLocal } from '../models/movie-local-storage';

@Injectable({
  providedIn: 'root'
})

export class StorageMoviesService extends MethodsLocalStorage {

  private _movieList: MovieLocal[] = [];
  private readonly _refreshMovies$ = new Subject<void>();

  get refreshMovies(){
    return this._refreshMovies$;
  }

  constructor() {
    super();
  }
  
  get<MovieLocal>(): Observable<MovieLocal[]> {
    let  movieList =  localStorage.getItem('movieList');
    return of(JSON.parse(movieList));
  }

  delete(id: number) {
    if(this._movieList){
      this._movieList = this._movieList.filter(movie => movie.id !== id);
      localStorage.setItem('movieList', JSON.stringify(this._movieList))
      this._refreshMovies$.next();
    }
    else{
      let  movieList:string =  localStorage.getItem('movieList');
      let movieListParsed = JSON.parse(movieList);
      this._movieList = movieListParsed.filter(movie => movie.id !== id);
      localStorage.setItem('movieList', JSON.stringify(this._movieList))
      this._refreshMovies$.next();
    }
    
  }
  
  add(movie: MovieLocal) {
    let actortest =  this._movieList.find( movieL =>  movieL.title === movie.title );
    if(!actortest){
      this._movieList.push(movie); 
      localStorage.setItem('movieList', JSON.stringify(this._movieList))
    }
  }



}
