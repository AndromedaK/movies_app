/**MODULES */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from "./material/material.module";
import { HttpClientModule } from "@angular/common/http";
import { RatingModule } from 'ng-starrating';
import { PipesModule } from "./pipes/pipes.module";
import { AuthModule } from "./components/auth/auth.module";


/**COMPONENTS */
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';
import { ParticlesModule } from 'angular-particle';


@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    RatingModule,
    PipesModule,
    AuthModule,
    ParticlesModule
  ], 
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
 