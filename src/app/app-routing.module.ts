import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';



const routes: Routes = [
  { 
    path: '', 
    redirectTo: 'auth/login',
    pathMatch: 'full'
  },
  {
    path: 'auth',
    loadChildren: ()=> import("./components/auth/auth.module").then(m=>m.AuthModule),
  },
  {
    path: '',
    canLoad: [AuthGuard],
    loadChildren: () => import("./components/layout/layout.module").then(m=>m.LayoutModule)
  },
  {
    path: '**',
    component: PageNotFoundComponent
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
